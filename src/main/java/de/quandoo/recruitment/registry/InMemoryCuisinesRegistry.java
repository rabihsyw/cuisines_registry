package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<Customer, Set<Cuisine>> customers;

    // here a repository can be injected for better design and testable code.
    public InMemoryCuisinesRegistry() {
        customers = new Hashtable<>();
    }

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        Set<Cuisine> cuisines = customers.computeIfAbsent(customer, k -> new HashSet<>());
        cuisines.add(cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return customers.entrySet().stream()
                .filter(c -> c.getValue().contains(cuisine))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return customers.entrySet().stream()
                .filter(e -> e.getKey().equals(customer))
                .flatMap(e-> e.getValue().stream())
                .collect(Collectors.toList());
    }

    /* for millions of customers/cuisines we can move the next code to an async background process to calculate
       top cuisines and the method 'topCuisines' will return that calculated result with O(1).
     */
    @Override
    public List<Cuisine> topCuisines(final int limit) {
        return customers.entrySet().stream().map(Map.Entry::getValue)
            .flatMap(Collection::stream)
            .collect(groupingBy(cuisine -> cuisine))
            .entrySet().stream()
            .sorted((e, e2) -> e.getValue().size() >= e2.getValue().size() ? -1 : 1)
            .limit(limit)
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());
    }
}
