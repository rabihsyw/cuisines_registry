package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    private static final Customer CUSTOMER_1 = new Customer("1");
    private static final Customer CUSTOMER_2 = new Customer("2");
    private static final Customer CUSTOMER_3 = new Customer("3");
    private static final Customer CUSTOMER_4 = new Customer("4");
    private static final Cuisine CUISINE_GERMAN = new Cuisine("German");
    private static final Cuisine CUISINE_SYRIAN = new Cuisine("Syrian");
    private static final Cuisine CUISINE_INDIAN = new Cuisine("Indian");
    private static final Cuisine CUISINE_ITALIAN = new Cuisine("Italian");

    @Before
    public void setUp() throws Exception {
        cuisinesRegistry.register(CUSTOMER_1, CUISINE_GERMAN);
        cuisinesRegistry.register(CUSTOMER_1, CUISINE_INDIAN);
        cuisinesRegistry.register(CUSTOMER_2, CUISINE_SYRIAN);
        cuisinesRegistry.register(CUSTOMER_2, CUISINE_INDIAN);
        cuisinesRegistry.register(CUSTOMER_2, CUISINE_ITALIAN);
        cuisinesRegistry.register(CUSTOMER_3, CUISINE_ITALIAN);
        cuisinesRegistry.register(CUSTOMER_3, CUISINE_INDIAN);
        cuisinesRegistry.register(CUSTOMER_4, CUISINE_GERMAN);
        cuisinesRegistry.register(CUSTOMER_4, CUISINE_SYRIAN);
        cuisinesRegistry.register(CUSTOMER_4, CUISINE_INDIAN);
        cuisinesRegistry.register(CUSTOMER_4, CUISINE_ITALIAN);
    }

    @Test
    public void getCuisineCustomersShouldReturn3Customers() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(CUISINE_INDIAN);
        assertFalse(customers.isEmpty());
        assertEquals(4, customers.size());
    }

    @Test
    public void getCuisineCustomersShouldReturn2Customers() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(CUISINE_SYRIAN);
        assertFalse(customers.isEmpty());
        assertEquals(2, customers.size());
    }

    @Test
    public void getCuisineCustomersShouldReturnEmptyList() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("Spanish"));
        assertTrue(customers.isEmpty());
    }

    @Test
    public void getCuisineCustomersWithNullCuisineShouldReturnEmptyList() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
        assertTrue(customers.isEmpty());
    }

    @Test
    public void getCustomerCuisinesWithNullCustomerShouldReturnEmptyList() {
        assertTrue(cuisinesRegistry.customerCuisines(null).isEmpty());
    }

    @Test
    public void getCustomerCuisinesShouldReturnEmptyList() {
        assertTrue(cuisinesRegistry.customerCuisines(new Customer("10")).isEmpty());
    }

    @Test
    public void getCustomerCuisinesShouldReturn2Cuisine() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(CUSTOMER_1);
        assertEquals(2, cuisines.size());
    }

    @Test
    public void getCustomerCuisinesShouldReturn4Cuisine() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(CUSTOMER_4);
        assertEquals(4, cuisines.size());
    }

    @Test
    public void getTopCuisinesShouldReturnIndianCuisine() {
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(1);
        assertEquals(1, cuisines.size());
        assertEquals(CUISINE_INDIAN, cuisines.iterator().next());
    }

    @Test
    public void getTopCuisinesShouldReturn3Cuisines() {
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);
        assertEquals(2, cuisines.size());
        Iterator<Cuisine> iterator = cuisines.iterator();
        assertEquals(CUISINE_INDIAN, iterator.next());
        assertEquals(CUISINE_ITALIAN, iterator.next());
    }


}